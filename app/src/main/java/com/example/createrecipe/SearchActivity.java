package com.example.createrecipe;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class SearchActivity extends AppCompatActivity {

    private Spinner mSearchField;
    private ImageButton mSearchBtn;

    private RecyclerView recyclerView;
    private ListView listView;

    private DatabaseReference databaseReference;
    ArrayList<Recipe> recipeList;
    RecipeListNewAdapter recipeListAdapter;
    ArrayAdapter<Recipe> adapter;

    int pos;

    DatabaseReference query;

    String[] recipeType = {"All","Breakfast", "Lunch", "Dinner"};

//    private void initializeView(){
//        mSearchField = (Spinner)findViewById(R.id.search_field);
//        mSearchField.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, recipeType));
//
//        listView = (ListView) findViewById(R.id.result_list);
//        listView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getRecipe()));
//
//        mSearchField.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if(position >= 0 && position < recipeType.length){
//                    getSelectedData(position);
//                }else{
//                    Toast.makeText(SearchActivity.this, "NOT EXIST", Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }
//
//    private ArrayList<RecipeBody> getRecipe(){
//        final ArrayList<RecipeBody> data = new ArrayList<>();
//        data.clear();
//
//
//
//        databaseReference = FirebaseDatabase.getInstance().getReference("recipe");
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                recipeList = new ArrayList<RecipeBody>();
//                for (DataSnapshot recipeSnapshot : dataSnapshot.getChildren()){
//                    final RecipeBody recipe = recipeSnapshot.getValue(RecipeBody.class);
//                    data.add(recipe);
//                }
//            }
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//
//        return data;
//    }
//
//    private void getSelectedData(int categoryID){
//        ArrayList<RecipeBody> recipeList = new ArrayList<>();
//        if(categoryID == 0){
//            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getRecipe());
//        }else{
//            for(RecipeBody recipe : getRecipe()){
//                if(recipe.getRecipeType().equalsIgnoreCase("breakfast")){
//                    int pos = 1;
//                    if(pos == categoryID){
//                        recipeList.add(recipe);
//                    }
//                }else if(recipe.getRecipeType().equalsIgnoreCase("lunch")){
//                    int pos = 2;
//                    if(pos ==categoryID){
//                        recipeList.add(recipe);
//                    }
//                }else if(recipe.getRecipeType().equalsIgnoreCase("dinner")){
//                    int pos = 3;
//                    if(pos ==categoryID){
//                        recipeList.add(recipe);
//                    }
//                }
//            }
//            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, recipeList);
//        }
//
//        listView.setAdapter(adapter);
//    }
//
//    class RecipeBody{
//        String recipeId;
//        String recipeName;
//        String ingredient;
//        String step;
//        String recipeType;
//        String imageUrl;
//
//        public RecipeBody(){}
//
//        public RecipeBody(String recipeId, String recipeName, String ingredient, String step, String recipeType, String imageUrl){
//            this.recipeId = recipeId;
//            this.recipeName = recipeName;
//            this.ingredient = ingredient;
//            this.step = step;
//            this.recipeType = recipeType;
//            this.imageUrl = imageUrl;
//        }
//
//        public String getRecipeId(){
//            return recipeId;
//        }
//
//        public String getRecipeName(){
//            return recipeName;
//        }
//
//        public String getIngredient(){
//            return ingredient;
//        }
//
//        public String getStep(){
//            return step;
//        }
//
//        public String getRecipeType(){
//            return recipeType;
//        }
//
//        public String getImageUrl(){ return imageUrl; }
//
//
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

//        initializeView();

        mSearchField = (Spinner) findViewById(R.id.search_field);
        mSearchBtn = (ImageButton) findViewById(R.id.search_btn);

        query =  FirebaseDatabase.getInstance().getReference("recipe");

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView = (RecyclerView) findViewById(R.id.result_list);
//        listView = (ListView) findViewById(R.id.result_list);
        recyclerView.setLayoutManager(layoutManager);


        databaseReference = FirebaseDatabase.getInstance().getReference("recipe");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                recipeList = new ArrayList<Recipe>();
                for (DataSnapshot recipeSnapshot : dataSnapshot.getChildren()){
                    final Recipe recipe = recipeSnapshot.getValue(Recipe.class);
                    recipeList.add(recipe);
                }

                recipeListAdapter = new RecipeListNewAdapter(SearchActivity.this, recipeList);
                recyclerView.setAdapter(recipeListAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



}
