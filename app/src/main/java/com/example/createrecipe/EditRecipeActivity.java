package com.example.createrecipe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditRecipeActivity extends AppCompatActivity {

    EditText eName, eIngredient, eStep;
    Spinner eType;
    Button eButton;
    String eUrl;
    String recipeiId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_recipe);

        eName = (EditText) findViewById(R.id.editTextEditName);
        eIngredient = (EditText) findViewById(R.id.editTextEditIngredient);
        eStep = (EditText) findViewById(R.id.editTextEditStep);

        eType = (Spinner) findViewById(R.id.spinnerTypeEditType);
        eButton = (Button) findViewById(R.id.buttonEdit);


        eButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = eName.getText().toString().trim();
                String ingredient = eIngredient.getText().toString();
                String step = eStep.getText().toString();
                String type = eType.getSelectedItem().toString();

                if(TextUtils.isEmpty(name)){
                    eName.setError("Recipe Name Required");
                    return;
                }else if(TextUtils.isEmpty(ingredient)){
                    eIngredient.setError("Ingredient Required");
                    return;
                }else if(TextUtils.isEmpty(step)){
                    eStep.setError("Step Required");
                    return;
                }

                updateRecipe(recipeiId, name, ingredient, step, type, eUrl);
            }
        });

        getIncomingIntent();

    }

    private boolean updateRecipe(String id, String name, String ingredient, String step, String type, String url){

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("recipe").child(id);
        Recipe recipe = new Recipe(id, name, ingredient, step, type, url);
        databaseReference.setValue(recipe);
        Toast.makeText(this, "Update Successfully", Toast.LENGTH_LONG).show();
        finish();
        return true;
    }

    private void getIncomingIntent(){
        if(getIntent().hasExtra("recipe_ingredients_edit") && getIntent().hasExtra("recipe_steps_edit")
                && getIntent().hasExtra("recipe_name_edit") && getIntent().hasExtra("recipe_type_edit")){
            String recipeNameEdit = getIntent().getStringExtra("recipe_name_edit");
            String recipeIngredientEdit = getIntent().getStringExtra("recipe_ingredients_edit");
            String recipeStepEdit = getIntent().getStringExtra("recipe_steps_edit");
            String recipeTypeEdit = getIntent().getStringExtra("recipe_type_edit").trim();
            eUrl = getIntent().getStringExtra("recipe_url_edit");
            recipeiId = getIntent().getStringExtra("recipe_id_edit");

            setDetail(recipeiId,recipeNameEdit, recipeIngredientEdit, recipeStepEdit, recipeTypeEdit, eUrl);
        }
    }

    private void setDetail(String recipeIdE, String recipeNameE, String recipeIngredientE, String recipeStepE, String recipeTypeE, String recipeImageE){
        TextView rNameEdit = findViewById(R.id.editTextEditName);
        TextView rIngredientEdit = findViewById(R.id.editTextEditIngredient);
        TextView rStepEdit = findViewById(R.id.editTextEditStep);
        Spinner rTypeEdit = findViewById(R.id.spinnerTypeEditType);

        rNameEdit.setText(recipeNameE);
        rIngredientEdit.setText(recipeIngredientE);
        rStepEdit.setText(recipeStepE);

//        rTypeEdit.setSelection(2);

//        Log.d("myTag", recipeTypeE);

        if(recipeTypeE.equalsIgnoreCase("Breakfast")){
            rTypeEdit.setSelection(0);
        }else if (recipeTypeE.equalsIgnoreCase("Lunch")){
            rTypeEdit.setSelection(1);
        }else if(recipeTypeE.equalsIgnoreCase("Dinner")){
            rTypeEdit.setSelection(2);
        }

    }

}
