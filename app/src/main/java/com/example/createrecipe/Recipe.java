package com.example.createrecipe;

public class Recipe {

    String recipeId;
    String recipeName;
    String ingredient;
    String step;
    String recipeType;
    String imageUrl;

    public Recipe(){}

    public Recipe(String recipeId, String recipeName, String ingredient, String step, String recipeType, String imageUrl){
        this.recipeId = recipeId;
        this.recipeName = recipeName;
        this.ingredient = ingredient;
        this.step = step;
        this.recipeType = recipeType;
        this.imageUrl = imageUrl;
    }

    public String getRecipeId(){
        return recipeId;
    }

    public String getRecipeName(){
        return recipeName;
    }

    public String getIngredient(){
        return ingredient;
    }

    public String getStep(){
        return step;
    }

    public String getRecipeType(){
        return recipeType;
    }

    public String getImageUrl(){ return imageUrl; }

}
