package com.example.createrecipe;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DetailsActivity extends AppCompatActivity {

    String recipeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ImageButton buttonEdit, buttonDelete;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Ingredients"));
        tabLayout.addTab(tabLayout.newTab().setText("Steps by Steps"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        buttonEdit = (ImageButton)findViewById(R.id.ImageButtonEdit);
        buttonDelete = (ImageButton) findViewById(R.id.ImageButtonDelete);

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRecipe(recipeId);
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DetailsActivity.this, EditRecipeActivity.class);
                intent.putExtra("recipe_id_edit", getIntent().getStringExtra("recipe_id"));
                intent.putExtra("recipe_ingredients_edit", getIntent().getStringExtra("recipe_ingredients"));
                intent.putExtra("recipe_steps_edit", getIntent().getStringExtra("recipe_steps"));
                intent.putExtra("recipe_name_edit", getIntent().getStringExtra("recipe_name"));
                intent.putExtra("recipe_type_edit", getIntent().getStringExtra("recipe_type"));
                startActivity(intent);
//                Intent intent = new Intent(DetailsActivity.this, EditRecipeActivity.class);
//                startActivity(intent);
            }
        });

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        getIncomingIntent();
    }

    private void deleteRecipe(String artistid){

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("recipe").child(artistid);

        databaseReference.removeValue();

        Toast.makeText(this, "Delete Successfully", Toast.LENGTH_LONG).show();
        finish();
    }

    private void getIncomingIntent(){
        if(getIntent().hasExtra("recipe_ingredients") && getIntent().hasExtra("recipe_steps")
                && getIntent().hasExtra("recipe_name") && getIntent().hasExtra("recipe_type")){
            String recipeName = getIntent().getStringExtra("recipe_name");
            String recipeIngredient = getIntent().getStringExtra("recipe_ingredients");
            String recipeStep = getIntent().getStringExtra("recipe_steps");
            String recipeType = getIntent().getStringExtra("recipe_type");
            recipeId = getIntent().getStringExtra("recipe_id");
            String recipeUrl = getIntent().getStringExtra("recipe_url");

            setDetail(recipeName, recipeIngredient, recipeStep, recipeType, recipeUrl);
        }
    }

    private void setDetail(String recipeName, String recipeIngredient, String recipeStep, String recipeType, String recipeUrl){
        TextView rName = findViewById(R.id.textViewRecipeN);
//        TextView rIngredient = findViewById(R.id.textViewIngredient);
//        TextView rStep = findViewById(R.id.textViewStep);
        TextView rType = findViewById(R.id.textViewT);
        ImageView rUrl = findViewById(R.id.imageView2);

        rName.setText(recipeName);
//        rIngredient.setText(recipeIngredient);
//        rStep.setText(recipeStep);
        rType.setText(recipeType);
        Glide.with(getApplicationContext()).load(recipeUrl).into(rUrl);

    }
}
