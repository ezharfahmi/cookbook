package com.example.createrecipe;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.MyViewHolder> {


    private static final String TAG = "RecipeListAdapter";

    Context context;
    ArrayList<Recipe> recipes;

    public RecipeListAdapter(Context c, ArrayList<Recipe> r){
        context = c;
        recipes = r;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.list_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position){
//        holder.ingredient.setText(recipes.get(position).getIngredient());
//        holder.step.setText(recipes.get(position).getStep());
        holder.recipeN.setText(recipes.get(position).getRecipeName());
        holder.type.setText(recipes.get(position).getRecipeType());

        if(recipes.get(position).getRecipeType().equalsIgnoreCase("breakfast")){
            holder.foodImage.setImageResource(R.drawable.breakfast);
        }else if(recipes.get(position).getRecipeType().equalsIgnoreCase("lunch")){
            holder.foodImage.setImageResource(R.drawable.lunch);
        }else if(recipes.get(position).getRecipeType().equalsIgnoreCase("dinner")){
            holder.foodImage.setImageResource(R.drawable.dinner);
        }

//        Glide.with(context).load(recipes.get(position).getImageUrl()).into(holder.foodImage);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("recipe_id", recipes.get(position).getRecipeId());
                intent.putExtra("recipe_ingredients", recipes.get(position).getIngredient());
                intent.putExtra("recipe_steps", recipes.get(position).getStep());
                intent.putExtra("recipe_name", recipes.get(position).getRecipeName());
                intent.putExtra("recipe_type", recipes.get(position).getRecipeType());
                intent.putExtra("recipe_url", recipes.get(position).getImageUrl());

                SharedPreferences prefs;
                prefs = context.getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("recipe_ingredients", recipes.get(position).getIngredient());
                editor.putString("recipe_steps", recipes.get(position).getStep());
                editor.apply();

                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount(){
        return recipes.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView ingredient, step,type, recipeN;
        LinearLayout parentLayout;
        ImageView foodImage;

        public MyViewHolder(View itemView){
            super(itemView);
//            ingredient = (TextView) itemView.findViewById(R.id.textViewIngredient);
//            step = (TextView) itemView.findViewById(R.id.textViewStep);

            type = (TextView) itemView.findViewById(R.id.textViewType);
            recipeN = (TextView) itemView.findViewById(R.id.textViewRecipeName);
            parentLayout = (LinearLayout) itemView.findViewById(R.id.parent_layout);
            foodImage = (ImageView)itemView.findViewById(R.id.displayFood);
        }

        public void onClick(final int position){

        }
    }
}
