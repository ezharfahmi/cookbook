package com.example.createrecipe;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static android.content.Context.MODE_PRIVATE;

public class TabFragment1 extends Fragment {

    TextView ingredient1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        SharedPreferences prefs = this.getActivity().getSharedPreferences("MYPREFS", MODE_PRIVATE);
        String getIngredient = prefs.getString("recipe_ingredients", "Ingredients");

        LayoutInflater lf = getActivity().getLayoutInflater();
        View view =  lf.inflate(R.layout.tab_fragment_1, container, false);

        ingredient1 = (TextView) view.findViewById(R.id.textViewIng);

        ingredient1.setText(getIngredient);

        return view;

    }
}
