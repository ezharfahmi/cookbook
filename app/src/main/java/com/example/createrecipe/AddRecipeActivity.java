package com.example.createrecipe;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.UUID;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class AddRecipeActivity extends AppCompatActivity {

    EditText editTextIngredients;
    EditText editTextSteps;
    EditText editTextRecipeName;
    Button buttonAddRecipe;
    Button buttonTakePic;
    Spinner spinnerType;
    ImageView imageView;
    private Uri filePath;

    private final int PICK_IMAGE_REQUEST = 71;

    String Storage_Path = "All_Image_Uploads/";

    Bitmap selectedImage;

    Bitmap takenImage;

    DatabaseReference databaseReference;
    FirebaseAuth mAuth = FirebaseAuth.getInstance();

    FirebaseStorage storage;
    StorageReference storageReference;

    ProgressDialog progressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipe);

        databaseReference = FirebaseDatabase.getInstance().getReference("recipe");

        editTextIngredients = (EditText) findViewById(R.id.editTextIngredient);
        editTextSteps = (EditText) findViewById(R.id.editTextStep);
        editTextRecipeName = (EditText) findViewById(R.id.editTextRecipeName);
        buttonAddRecipe = (Button) findViewById(R.id.buttonAddRecipe);
        buttonTakePic = (Button) findViewById(R.id.buttonTakePic);
        spinnerType = (Spinner) findViewById(R.id.spinnerType);
        imageView = (ImageView) findViewById(R.id.foodImage);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        buttonTakePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(AddRecipeActivity.this);
            }
        });

        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            buttonAddRecipe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uploadImage();
                }
            });
        } else {
            signInAnonymously();
        }
    }

    private void signInAnonymously(){
        mAuth.signInAnonymously().addOnSuccessListener(this, new OnSuccessListener<AuthResult>() {
            @Override public void onSuccess(AuthResult authResult) {
                buttonAddRecipe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        uploadImage();
                    }
                });
            }
        }) .addOnFailureListener(this, new OnFailureListener() {
            @Override public void onFailure(@NonNull Exception exception) {
                Log.e("TAG", "signInAnonymously:FAILURE", exception);
            }
        });
    }


    private void selectImage(Context context) {
        final CharSequence[] options = { "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your recipe picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void uploadImage() {

        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final StorageReference ref = storageReference.child("images/"+ UUID.randomUUID().toString());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String ingredients = editTextIngredients.getText().toString().trim();
                                    String steps = editTextSteps.getText().toString().trim();
                                    String recipeName = editTextRecipeName.getText().toString().trim();
                                    String typeRecipe = spinnerType.getSelectedItem().toString();

                                    String id = databaseReference.push().getKey();
                                    Recipe recipe = new Recipe(id, recipeName, ingredients, steps, typeRecipe, uri.toString());
                                    databaseReference.child(id).setValue(recipe);

                                    progressDialog.dismiss();
                                    Toast.makeText(AddRecipeActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            });

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(AddRecipeActivity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }

    private void addRecipe(){
        String ingredients = editTextIngredients.getText().toString().trim();
        String steps = editTextSteps.getText().toString().trim();
        String recipeName = editTextRecipeName.getText().toString().trim();
        String typeRecipe = spinnerType.getSelectedItem().toString();

        if(!TextUtils.isEmpty(ingredients) && !TextUtils.isEmpty(steps) && !TextUtils.isEmpty(typeRecipe) && !TextUtils.isEmpty(recipeName)){
            String id = databaseReference.push().getKey();

            Recipe recipe = new Recipe(id, recipeName, ingredients, steps, typeRecipe, filePath.toString());

            databaseReference.child(id).setValue(recipe);

            Toast.makeText(this, "Success Added", Toast.LENGTH_LONG).show();


        }else{
            Toast.makeText(this, "Please Enter all the field", Toast.LENGTH_LONG).show();
        }
    }
}
