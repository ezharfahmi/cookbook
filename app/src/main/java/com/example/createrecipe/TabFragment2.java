package com.example.createrecipe;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static android.content.Context.MODE_PRIVATE;

public class TabFragment2 extends Fragment {

    TextView ingredient2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        SharedPreferences prefs = this.getActivity().getSharedPreferences("MYPREFS", MODE_PRIVATE);
        String getStep = prefs.getString("recipe_steps", "Steps");

        LayoutInflater lf = getActivity().getLayoutInflater();
        View view =  lf.inflate(R.layout.tab_fragment_2, container, false);

        ingredient2 = (TextView) view.findViewById(R.id.textViewSte);

        ingredient2.setText(getStep);

        return view;
    }
}
